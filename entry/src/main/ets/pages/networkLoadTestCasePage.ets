/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { GIFComponent, ResourceLoader } from '@ohos/gif-drawable'


@Entry
@Component
struct NetworkLoadTestCasePage {
  @State model1Page8: GIFComponent.ControllerOptions = new GIFComponent.ControllerOptions();
  @State gifAutoPlay1Page8: boolean = true;
  @State gifReset1Page8: boolean = false;
  @State hint1: string = 'gif1是否解析完成？';
  @State hint11: string = '当前GIF循环了多少次？';
  @State hint111: string = '网络加载并解析耗时ms';
  gif1LoopCount = 0;
  @State model2Page8: GIFComponent.ControllerOptions = new GIFComponent.ControllerOptions();
  @State gifAutoPlay2Page8: boolean = true;
  @State gifReset2Page8: boolean = false;
  @State hint2: string = 'gif2是否解析完成？';
  @State hint22: string = '当前GIF循环了多少次？';
  @State hint222: string = '网络加载并解析耗时ms';
  gif2LoopCount = 0;
  @State model3Page8: GIFComponent.ControllerOptions = new GIFComponent.ControllerOptions();
  @State gifAutoPlay3Page8: boolean = true;
  @State gifReset3Page8: boolean = false;
  @State hint3: string = 'gif3是否解析完成？';
  @State hint33: string = '当前GIF循环了多少次？';
  @State hint333: string = '网络加载并解析耗时ms';
  gif3LoopCount = 0;

  canClick1:boolean = true;
  canClick2:boolean = true;
  canClick3:boolean = true;

  build() {
    Scroll() {
      Column() {
        Column() {
          Text(this.hint1)
          Text(this.hint11)
          Text(this.hint111)

          Button('点击绘制GIF1')
            .onClick(() => {
              if(this.canClick1){
                this.canClick1 = false;
                this.gifAutoPlay1Page8 = false;
                this.model1Page8.destroy();

                let s1 = new Date().getTime();
                let modelx = new GIFComponent.ControllerOptions()
                modelx
                  .setLoopFinish(() => {
                    this.gif1LoopCount++;
                    this.hint11 = '当前gif循环了' + this.gif1LoopCount + '次'
                  })

                  .setScaleType(GIFComponent.ScaleType.FIT_CENTER)
                  .setSpeedFactor(1)
                  .setOpenHardware(false)


                ResourceLoader.downloadDataWithContext(getContext(this), {
                  url: 'https://gd-hbimg.huaban.com/e0a25a7cab0d7c2431978726971d61720732728a315ae-57EskW_fw658'
                }, (sucBuffer) => {
                  modelx.loadBuffer(sucBuffer, () => {
                    let e1 = new Date().getTime();
                    this.hint111 = '网络加载并解析耗时' + (e1 - s1) + 'ms'
                    console.log('网络加载解析成功回调绘制！')
                    this.gifAutoPlay1Page8 = true;
                    this.model1Page8 = modelx;
                    this.canClick1 = true;
                  })
                }, (err) => {
                })
              }
            })

          Button('点击开始播放')
            .onClick(() => {
              this.gifAutoPlay1Page8 = true;
            })
          Button('点击停止播放')
            .onClick(() => {
              this.gifAutoPlay1Page8 = false;
            })
          Button('点击重置播放')
            .onClick(() => {
              console.log('点击重置播放1111')
              this.gifReset1Page8 = !this.gifReset1Page8;
            })
          GIFComponent({ model: $model1Page8, autoPlay: $gifAutoPlay1Page8, resetGif: $gifReset1Page8 })
            .width(200).height(200)
        }.backgroundColor(Color.Gray)

        Column() {
          Text(this.hint2)
          Text(this.hint22)
          Text(this.hint222)
          Button('点击绘制GIF2')
            .onClick(() => {
              if(this.canClick2){
                this.canClick2 = false

              this.gifAutoPlay2Page8 = false;
              this.model2Page8.destroy();
              let s2 = new Date().getTime();
              let modelx = new GIFComponent.ControllerOptions()
              modelx
                .setLoopFinish(() => {
                  this.gif2LoopCount++;
                  this.hint22 = '当前gif循环了' + this.gif2LoopCount + '次'
                })

                .setScaleType(GIFComponent.ScaleType.FIT_XY)
                .setSpeedFactor(3)
                .setOpenHardware(false)

              ResourceLoader.downloadDataWithContext(getContext(this), {
                url: 'https://pic.ibaotu.com/gif/18/17/16/51u888piCtqj.gif!fwpaa70/fw/700'
              }, (sucBuffer) => {
                modelx.loadBuffer(sucBuffer, () => {
                  let e2 = new Date().getTime();
                  this.hint222 = '网络加载并解析耗时' + (e2 - s2) + 'ms'
                  console.log('网络加载解析成功回调绘制！')
                  this.gifAutoPlay2Page8 = true;
                  this.model2Page8 = modelx;
                  this.canClick2 = true;
                })
              }, (err) => {
              })
              }
            })

          Button('点击开始播放')
            .onClick(() => {
              this.gifAutoPlay2Page8 = true;
            })
          Button('点击停止播放')
            .onClick(() => {
              this.gifAutoPlay2Page8 = false;
            })
          Button('点击重置播放')
            .onClick(() => {
              this.gifReset2Page8 = !this.gifReset2Page8;
            })
          GIFComponent({ model: $model2Page8, autoPlay: $gifAutoPlay2Page8, resetGif: $gifReset2Page8 })
            .width(200).height(200)
        }.backgroundColor(Color.Yellow)

        Column() {
          Text(this.hint3)
          Text(this.hint33)
          Text(this.hint333)
          Button('点击绘制GIF3')
            .onClick(() => {
              if(this.canClick3){
                this.canClick3 = false;


              this.gifAutoPlay3Page8 = false;
              this.model3Page8.destroy();
              let s3 = new Date().getTime();
              let modelx = new GIFComponent.ControllerOptions()
              modelx
                .setLoopFinish(() => {
                  this.gif3LoopCount++;
                  this.hint33 = '当前gif循环了' + this.gif3LoopCount + '次'
                })

                .setScaleType(GIFComponent.ScaleType.FIT_END)
                .setSpeedFactor(3)
                .setOpenHardware(false)

              ResourceLoader.downloadDataWithContext(getContext(this), {
                url: 'https://res.vmallres.com/cmscdn/CN/2023-03/7052a601ac3e428c84c9415ad9734735.gif'
              }, (sucBuffer) => {
                modelx.loadBuffer(sucBuffer, () => {
                  let e3 = new Date().getTime();
                  this.hint333 = '网络加载并解析耗时' + (e3 - s3) + 'ms'
                  console.log('网络加载解析成功回调绘制！')
                  this.gifAutoPlay3Page8 = false;
                  this.model3Page8 = modelx;
                  this.canClick3 = true
                })
              }, (err) => {
              })
              }
            })

          Button('点击开始播放')
            .onClick(() => {

              this.gifAutoPlay3Page8 = true;
            })
          Button('点击停止播放')
            .onClick(() => {
              this.gifAutoPlay3Page8 = false;
            })
          Button('点击重置播放')
            .onClick(() => {
              this.gifReset3Page8 = !this.gifReset3Page8;
            })
          GIFComponent({ model: $model3Page8, autoPlay: $gifAutoPlay3Page8, resetGif: $gifReset3Page8 })
            .width(200).height(200)
        }.backgroundColor(Color.Pink)
      }
    }
    .height('100%').width('100%')
  }
}

